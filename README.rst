pv-simple-validator
===================

Very simple utility to validate EPICS PVs against the ESS Naming Convention string format.

Quickstart
----------

Python 3.6 or greater is required.

Note that you don't need to clone this repository.

.. code-block:: sh

   $ pip3 install --user pv-simple-validator -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple
   $ pv-simple-validator -h


Usage
-----

.. code-block:: sh

   $ pv-simple-validator Sys:Dis-Dev-1:Property --verbose
   $ pv-simple-validator -i tests/example_pvs.list
   $ cat tests/example_pvs.list | xargs pv-simple-validator -q


About
-----

ESS Naming Convention rules (as found on https://naming.esss.lu.se) are mostly defined in `rules`_.

.. _rules: psv/definition/rules.json

Finally
-------

This utility was written to be a simple and portable solution that only inspects the PV string format. A more advanced and flexible tool can be found at https://gitlab.esss.lu.se/icshwi/pvvalidator.
