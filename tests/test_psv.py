import pytest

from psv.main import check_length, validate_pv

from .utils import string_generator

VALID_PVS = [
    "The:Quick-Brown-100:Fox",
    "Jumps:Over-The-001:Lazy-Dog",
    "Ad:Lorum-Ipsum-999:Dolores",
    "Sys-Sub:Dis-Dev-0001:Property",
    "Sys-Sub:Dis-Dev-001:Property",
    "Sys-Sub:Dis-Dev-01:Property",
    "Sys-Sub:Dis-Dev-1:Property",
    "Sys-Sub:Dis-Dev-123abc:Property",
    "Sys:Dis-Dev-001:Property",
    f"{string_generator()}-{string_generator()}:{string_generator()}-{string_generator()}-1111:Property",
    f"Sys:Dis-Dev-1:{string_generator(size=25)}",
    "Sys:Dis-Dev-1:x",
    "Sys:Dis-Dev-1:ALLCAPS",
    "Sys:Dis-Dev-1:alllower",
    "Sys::Ch00-Test",
]

INVALID_PVS = [
    "Wrong-Type-Separator",
    "0123456789" * 5 + "1",
    "JustSomeRandomWords",
    "A place in the sky",
    "1 + 1 = 2",
    "WHY-NOT",
    "x",
    f"Sys:Dis-Dev-01:{string_generator(size=26)}",
    f"Sys:Dis-{string_generator(size=35)}-1:Property",
    "Sys:Dis-Dev-000:Property",
    "Sys:Dis-Dev-1:1Something",
    "Sys::00-Test",
]


@pytest.mark.parametrize(
    "string",
    (string_generator(size=size) for size in range(51, 61, 1)),
)
def test_string_is_too_long(string: str):
    assert check_length(string, max_length=50) is False


@pytest.mark.parametrize(
    "string",
    (string_generator(size=size) for size in range(1, 51, 1)),
)
def test_string_is_correct_length(string: str):
    assert check_length(string, max_length=50) is True


@pytest.mark.parametrize("pv", VALID_PVS)
def test_pv_is_valid(pv: str):
    assert validate_pv(pv) is True


@pytest.mark.parametrize("pv", INVALID_PVS)
def test_pv_is_invalid(pv: str):
    assert validate_pv(pv) is False
