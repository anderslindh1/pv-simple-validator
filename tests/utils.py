from random import choice
from string import ascii_letters


def string_generator(size: int = 6, chars: str = ascii_letters) -> str:
    return "".join(choice(chars) for _ in range(size))
