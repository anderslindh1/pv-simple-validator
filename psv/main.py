"""Uses rules (limits and regular expression) defined in ../definition/rules.json."""

import argparse
import json
import logging
import pathlib
import re
import sys


def check_length(string: str, min_length: int = 0, max_length: int = 6) -> bool:
    """Checks if string length is within boundaries."""
    return min_length < len(string) <= max_length


def validate_pv(pv: str) -> bool:
    """Validates PV string format against predefined rules."""
    package_path = pathlib.Path(__file__).parent.absolute()
    with open(f"{package_path}/definition/rules.json") as fh:
        rules: dict = json.load(fh)

    # check string format and separators
    element_groups = re.match(rules["string_format_regex"], pv)
    if not element_groups:
        logging.error(
            f"['{pv}'] - Does not follow the correct format ('Sys[-Sub]:Dis-Dev-<Index>:Property')"
        )
        return False

    *prefix_groups, _property = element_groups.groups()
    prefix_groups = [element for element in prefix_groups if element is not None]

    # check lengths
    for element in prefix_groups:
        element = element[1:] if element.startswith(("-", ":")) else element
        if not check_length(element, max_length=rules["element_length_max"]):
            logging.error(
                f"['{pv}'] - '{element}' is incorrect length ({rules['element_length_min']}-{rules['element_length_max']} characters allowed)"
            )
            return False
    if not check_length(_property, max_length=rules["property_length_max"]):
        allowed_with_contingency = (
            rules["property_length_max"] + rules["contingency_max"]
        )
        if check_length(_property, max_length=allowed_with_contingency):
            logging.warning(
                f"['{pv}'] - Property '{_property}' uses contingency for length"
            )
        else:
            logging.error(
                f"['{pv}'] - Property '{_property}' is too long (max is {allowed_with_contingency})"
            )
            return False

    # check digits
    if _property[0].isnumeric():
        logging.error(f"['{pv}'] - Property '{_property}' cannot start with a number")
        return False
    device_index = prefix_groups[-1]
    if re.match(r"^0+$", device_index):
        logging.error(
            f"['{pv}'] - Index '{device_index}' is incorrect (cannot be only 0s)"
        )
        return False

    # encourage good property names
    if len(_property) < 4 or (_property.isupper() or _property.islower()):
        logging.debug(
            f"['{pv}'] - Property '{_property}' should be clearly understable and use PascalCase"
        )

    logging.info(f"['{pv}'] - OK")
    return True


def main():
    parser = argparse.ArgumentParser(
        description="EPICS PV validator against ESS Naming Convention."
    )
    parser.add_argument("pvs", type=str, nargs="*", help="process variables")
    parser.add_argument(
        "-i", "--inputfile", type=str, help="path to file containing process variables"
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-v", "--verbose", action="store_true")
    group.add_argument("-q", "--quiet", action="store_true")
    args = parser.parse_args()

    if args.quiet:
        logging.basicConfig(level=logging.CRITICAL)
    elif args.verbose:
        logging.basicConfig(level=logging.INFO)
    else:
        logging.basicConfig(level=logging.WARNING)

    if args.pvs:
        pvs = args.pvs
    elif args.inputfile:
        with open(args.inputfile, "r") as f:
            pvs = f.readlines()

    results = [validate_pv(pv.strip()) for pv in pvs]
    sys.exit(0 if all(results) else 1)


if __name__ == "__main__":
    main()
